module.exports = (function() {
    'use strict';

    // Module dependencies
    var crypto  = require('crypto'),
        request = require('request'),
        nonce   = require('nonce')(),
    // moment = require('moment');
    moment = require('moment-timezone');


    // Constants
    var version         = '0.0.5',
        PUBLIC_API_URL  = 'https://poloniex.com/public',
        PRIVATE_API_URL = 'https://poloniex.com/tradingApi',
        USER_AGENT      = 'poloniex.js ' + version
        //USER_AGENT    = 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0'


    // Constructor
    function Poloniex(key, secret){
        // Generate headers signed by this user's key and secret.
        // The secret is encapsulated and never exposed
        this._getPrivateHeaders = function(parameters){
            var paramString, signature;

            if (!key || !secret){
                throw 'Poloniex: Error. API key and secret required';
            }

            // Sort parameters alphabetically and convert to `arg1=foo&arg2=bar`
            paramString = Object.keys(parameters).map(function(param){
                return encodeURIComponent(param) + '=' + encodeURIComponent(parameters[param]);
            }).join('&');

            signature = crypto.createHmac('sha512', secret).update(paramString).digest('hex');

            return {
                Key: key,
                Sign: signature
            };
        };
    }

    // Currently, this fails with `Error: CERT_UNTRUSTED`
    // Poloniex.STRICT_SSL can be set to `false` to avoid this. Use with caution.
    // Will be removed in future, once this is resolved.
    Poloniex.STRICT_SSL = true;

    // Helper methods
    function joinCurrencies(currencyA, currencyB){
        return currencyA + '_' + currencyB;
    }

    // Prototype
    Poloniex.prototype = {
        constructor: Poloniex,

        // Make an API request
        _request: function(options, callback){
            if (!('headers' in options)){
                options.headers = {};
            }

            options.headers['User-Agent'] = USER_AGENT;
            options.json = true;
            options.strictSSL = Poloniex.STRICT_SSL;

            request(options, function(err, response, body) {
                callback(err, body);
                // console.log(this);
            });

            return this;
        },

        // Make a public API request
        _public: function(parameters, callback){
            var options = {
                method: 'GET',
                url: PUBLIC_API_URL,
                qs: parameters
            };

            return this._request(options, callback);
        },

        // Make a private API request
        _private: function(parameters, callback){
            var options;

            parameters.nonce = nonce();
            options = {
                method: 'POST',
                url: PRIVATE_API_URL,
                form: parameters,
                headers: this._getPrivateHeaders(parameters)
            };

            return this._request(options, callback);
        },


        /////


        // PUBLIC METHODS

        getTicker: function(callback){
            var parameters = {
                    command: 'returnTicker'
                };

            return this._public(parameters, callback);
        },

        get24hVolume: function(callback){
            var parameters = {
                    command: 'return24hVolume'
                };

            return this._public(parameters, callback);
        },

        getOrderBook: function(currency, depth, callback){
            var parameters = {
                    command: 'returnOrderBook',
                    currencyPair: currency,
                    depth:depth
                };

            return this._public(parameters, callback);
        },

        getTradeHistory: function(currency, start, end, callback){
            var start_date = moment(start+"+0000",'YYYY-MM-DD HH:mm:ss Z').utc();
            var end_date = moment(end+"+0000",'YYYY-MM-DD HH:mm:ss Z').utc();
            var parameters = {
                    command: 'returnTradeHistory',
                    currencyPair: currency,
                    start: start_date.unix(),
                    end : end_date.unix()
                };
            return this._public(parameters, callback);
        },
        getChartData: function(currency, candlestick, start, end, callback){
            var start_date = moment(start+"+0000",'YYYY-MM-DD HH:mm:ss Z').utc();
            var end_date = moment(end+"+0000",'YYYY-MM-DD HH:mm:ss Z').utc();
            var parameters = {
                    command: 'returnChartData',
                    currencyPair: currency,
                    period: candlestick,
                    start: start_date.unix(),
                    end : end_date.unix()
                };

            return this._public(parameters, callback);
        },
        getCurrencies: function(callback) {
            var parameters = {
                command: 'returnTicker',
            };

            return this._public(parameters, callback);
        },

        // PRIVATE METHODS

        myBalances: function(callback){
            var parameters = {
                    command: 'returnBalances'
                };

            return this._private(parameters, callback);
        },
    myCompleteBalances: function(callback){
            var parameters = {
                    command: 'returnCompleteBalances'
                };

            return this._private(parameters, callback);
        },
    myTradableBalances: function(callback){
            var parameters = {
                    command: 'returnTradableBalances'
                };

            return this._private(parameters, callback);
        },


        myOpenOrders: function(currency, callback){
            var parameters = {
                    command: 'returnOpenOrders',
                    currencyPair: currency,
                };

            return this._private(parameters, callback);
        },

        myTradeHistory: function(currency, callback){
            var parameters = {
                    command: 'returnTradeHistory',
                    currencyPair: currency,
                };

            return this._private(parameters, callback);
        },

        buy: function(currency, rate, amount, callback){
            var parameters = {
                    command: 'buy',
                    // fillOrKill:'1',
                    // immediateOrCancel:'1',
                    currencyPair: currency,
                    rate: rate,
                    amount: amount
                };

            return this._private(parameters, callback);
        },

        sell: function(currency, rate, amount, callback){
            var parameters = {
                    command: 'sell',
                    // fillOrKill:'1',
                    currencyPair: currency,
                    rate: rate,
                    amount: amount
                };

            return this._private(parameters, callback);
        },

        cancelOrder: function(currency, orderNumber, callback){
            var parameters = {
                    command: 'cancelOrder',
                    currencyPair: currency,
                    orderNumber: orderNumber
                };

            return this._private(parameters, callback);
        },

        withdraw: function(currency, amount, address, callback){
            var parameters = {
                    command: 'withdraw',
                    currency: currency,
                    amount: amount,
                    address: address
                };

            return this._private(parameters, callback);
        },
        myMarginPosition: function(currency,callback){
            var parameters = {
                    command: 'getMarginPosition',
                    currencyPair: currency,
                };

            return this._private(parameters,callback);
        },
        marginBuy: function(currency,amount,rate,callback){
            var parameters = {
                    command: 'marginBuy',
                    currencyPair: currency,
                    rate : rate,
                    amount : amount,
                };
                console.log(parameters);

            return this._private(parameters, callback);
        },
        closeMarginPosition: function(currency,callback){
            var parameters = {
                    command: 'closeMarginPosition',
                    currencyPair: currency,
                };
                console.log(parameters);

            return this._private(parameters, callback);
        },
        marginSell: function(currency,amount,rate,callback){
            var parameters = {
                    command: 'marginSell',
                    currencyPair: currency,
                    rate : rate,
                    amount : amount,
                };
                console.log(parameters);

            return this._private(parameters, callback);
        },
    };

    return Poloniex;
})();