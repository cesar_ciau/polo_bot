var autobahn = require('autobahn');
var Poloniex = require('./lib/poloniex'),
    // When using as an NPM module, use `require('poloniex.js')`

    // Create a new instance, with optional API key and secret
    poloniex = new Poloniex(
        'OKUG5VSP-RPCVTYTS-9RUJFSDG-WIIQUD39',
        'a5b0684f62fb4863ce62c864b21a9532d49a2ce182e0f565a15b15f870bb95154de7fe60381f7a088d72bb3aa5ed927d2b103f6828ecd2245b3aa1d00b5c5eda'
    );
var wsuri = "wss://api.poloniex.com";
var ask = 0,ask_trade;
var bid = 0,bid_trade;
var eth_balance;
var btc_balance;
var orderNumber;
var price = 0;
var connection = new autobahn.Connection({
  url: wsuri,
  realm: "realm1"
});

var myBalance = function()
{
    var balance = new Promise(function(resolve, reject) {
        poloniex.myBalances(function(err, data){
            if (err){
                reject(err);
            }
            resolve(data)
        });
    });
    balance.then(function(resolve, reject){
        eth_balance = parseFloat(resolve.ETH);
        btc_balance = parseFloat(resolve.BTC);
    });
}

myBalance();
console.log("btc: "+btc_balance);
connection.onopen = function (session) {
    function tickerEvent (args,kwargs) {
        for (var i = 0; i < args.length; i++) {
            if (args[i] == "BTC_ETH") {
                price = args[i+1]
                ask = parseFloat(args[i+2]);
                bid = parseFloat(args[i+3]);
                // var ask_trade = ask - (ask * 0.00003);
                var spreed = ask - bid;
                var profit = (spreed / price) * 100;
                myBalance();
                if (profit > 0.2) {
                    console.log("btc: "+btc_balance);
                    console.log("profit: "+profit);
                    if (eth_balance < 0.0001 && btc_balance > 0.0001) {
                        bid_trade = bid + (bid * 0.0002);
                        ask_trade = bid_trade + (bid_trade * 0.0043);
                        var mount = parseFloat(btc_balance / price);
                        mount = mount - (mount * 0.2)
                        // mount = Math.round((mount * 100) - 10) / 100
                        var buy_promise = new Promise(function(resolve, reject) {
                            poloniex.buy("BTC_ETH",bid_trade,mount,function(err, data){
                                if (err){
                                    reject(err);
                                }
                                resolve(data)
                            });
                        });
                        buy_promise.then(function(resolve, reject){
                            console.log(resolve);
                            console.log("mount: "+mount);
                            if (resolve.orderNumber !== "") {
                                var sell_promise = new Promise(function(resolve, reject) {
                                    poloniex.sell("BTC_ETH",ask_trade,mount,function(err, data){
                                        if (err){
                                            reject(err);
                                        }
                                        resolve(data)
                                    });
                                });
                                sell_promise.then(function(resolve, reject){
                                    if (resolve.orderNumber !== "" && typeof resolve.orderNumber !== "undefined") {
                                        orderNumber = resolve.orderNumber;
                                    }
                                    console.log("sell 1");
                                });
                            }
                        });
                    } else {
                        if (eth_balance > 0.0001) {
                            var sell_promise = new Promise(function(resolve, reject) {
                                poloniex.sell("BTC_ETH",ask_trade,eth_balance,function(err, data){
                                    if (err){
                                        reject(err);
                                    }
                                    resolve(data)
                                });
                            });
                            sell_promise.then(function(resolve, reject){
                                console.log("sell 2");
                                console.log(resolve);
                            });
                        }
                    }
                }
                if (eth_balance > 0.0001) {
                    var sell_promise = new Promise(function(resolve, reject) {
                        poloniex.sell("BTC_ETH",ask_trade,eth_balance,function(err, data){
                            if (err){
                                reject(err);
                            }
                            resolve(data)
                        });
                    });
                    sell_promise.then(function(resolve, reject){
                        if (resolve.orderNumber !== "" && typeof resolve.orderNumber !== "undefined") {
                            orderNumber = resolve.orderNumber;
                        }
                        console.log("sell 3");
                        console.log(resolve);
                    });
                } else {
                    var stop = bid_trade - (bid_trade * 0.008);
                    var stop_loss = price - (price * 0.00012);
                    if (price <= stop) {
                        var open_balance = new Promise(function(resolve, reject) {
                            poloniex.myOpenOrders("BTC_ETH",function(err, data){
                                if (err){
                                    reject(err);
                                }
                                resolve(data)
                            });
                        });
                        open_balance.then(function(resolve, reject){
                            if (resolve.length > 1) {
                                if (typeof resolve.error == "undefined") {
                                    var sell_promise = new Promise(function(resolve, reject) {
                                        poloniex.sell("BTC_ETH",stop_loss,eth_balance,function(err, data){
                                            if (err){
                                                reject(err);
                                            }
                                            resolve(data)
                                        });
                                    });
                                    sell_promise.then(function(resolve, reject){
                                        if (resolve.orderNumber !== "" && typeof resolve.orderNumber !== "undefined") {
                                            orderNumber = resolve.orderNumber;
                                        }
                                        console.log("sell stop loss");
                                        console.log(resolve);
                                    });
                                }
                            }
                            // console.log(resolve);
                        });
                    }
                }
            }
        }
    }
    // session.subscribe('BTC_ETH', marketEvent);
    // session.subscribe('BTC_ETH', tradehistoryEvent);
    session.subscribe('ticker', tickerEvent);
    // session.subscribe('trollbox', trollboxEvent);
}

connection.onclose = function () {
  console.log("Websocket connection closed");
}
connection.open();