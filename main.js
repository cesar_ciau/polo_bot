var status = "Trade iniciado";
var TelegramBot = require('node-telegram-bot-api');

var token = '161348950:AAFlSbqkgriD09sSdCqW0J3-jFUY8MABJXY';
// Setup polling way
var bot = new TelegramBot(token, {polling: true});
var chatId = "";
var Poloniex = require('./lib/poloniex'),
Bitmex = require('./lib/bitmex'),
    // When using as an NPM module, use `require('poloniex.js')`

    // Create a new instance, with optional API key and secret
    poloniex = new Poloniex(
         '19EVXFQ5-APPNDBHS-MFDIP9QU-XPIIZU0Q',
	'bcddfa38c8dd1ea00ff3be802da81a366a87611f616b3b45a37cf990ea08c0847f3d42c81c49505b18593e858eb3e92adb4c45b2a758f0067418e85b54fe6ca8'
    );
var bitmex = new Bitmex();
var talib = require('./node_modules/talib/build/Release/talib');
var async = require('async');
moment = require('moment-timezone');
console.log("TALib Version: " + talib.version);
var lastBuy;
var profit = 0.026;


////////////////////Telegram api ////////////////////////////////
bot.onText(/\/start/, function (msg) {
    chatId = msg.chat.id;
    bot.sendMessage(chatId, 'Bot iniciado');
});
bot.onText(/\/status/, function (msg) {
    bot.sendMessage(chatId, status);
});
///////////////////////////////////////////////////

setInterval(function(){
    var toDay = moment().tz('Africa/Bissau').format("YYYY-MM-DD hh:mm:ss Z");
    var date_start_str = toDay;
    var date_base = dateBase(date_start_str);
    var marketData,price_market,macd_data,macd2_data,short,long,diff,position,rsi,trade_buy,tradeable_balance,last_buy,short,long,open_orders,macd2 = {short:"",long:"",diff:""};
    // console.log("to day"+toDay);
    async.series([
        function(callback){
            var open_orders_promise = new Promise(function(resolve, reject) {
                poloniex.myOpenOrders("BTC_ETH",function(err, data){
                    if (err){
                        reject(err);
                    }
                    resolve(data)
                });
            });
            open_orders_promise.then(function(resolve, reject){
                open_orders = resolve;
                callback();
            });
        },
        function(callback){
            if (open_orders.length > 0) {
                for (var i = 0; i < open_orders.length; i++) {
                    var orderNumber = open_orders[i].orderNumber;
                    var cancel_orders_promise = new Promise(function(resolve, reject) {
                        poloniex.cancelOrder("BTC_ETH",orderNumber,function(err, data){
                            if (err){
                                reject(err);
                            }
                            resolve(data)
                        });
                    });
                    cancel_orders_promise.then(function(resolve, reject){
                        callback();
                    });
                }
            } else {
                callback();
            }
        },
        function(callback) {
            var price_promise = new Promise(function(resolve, reject) {
                bitmex.getTicker(".ETHXBT","500",function(err, data){
                    if (err){
                        reject(err);
                    }
                    var marketData = formatMarketBF(data);
                    resolve(marketData)
                });
            });
            price_promise.then(function(resolve, reject){
                if (resolve.length > 0) {
                    marketData = resolve
                }
                callback();
            });
        },
        function(callback){
            var price_market_promise = new Promise(function(resolve, reject) {
                poloniex.getCurrencies(function(err, data){
                    if (err){
                        reject(err);
                    }
                    resolve(data)
                });
            });
            price_market_promise.then(function(resolve, reject){
                price_market = resolve.BTC_ETH.last
                callback();
            });
        },
        function(callback){
            var macd_promise = new Promise(function(resolve, reject) {
                talib.execute({
                    name: "MACD",
                    startIdx: 0,
                    endIdx: marketData.length-1,
                    inReal: marketData,
                    optInFastPeriod: 12,
                    optInSlowPeriod: 26,
                    optInSignalPeriod: 9
                }, function (result) {
                    var length = result.result.outMACD.length-1;
                    var macd = {
                        macd:result.result.outMACD[length],
                        signal:result.result.outMACDSignal[length],
                        histogram:result.result.outMACDHist[length],
                    };
                    resolve(macd);
                });
            });
            macd_promise.then(function(resolve, reject){
                macd_data = resolve
                callback();
            });
        },
        function(callback) {
            var margin_position_promise = new Promise(function(resolve, reject) {
                poloniex.myMarginPosition('BTC_ETH',function(err, data){
                    if (err){
                        reject(err);
                    }
                    resolve(data);
                });
            });
            margin_position_promise.then(function(resolve, reject){
                position = resolve
                callback();
            });
        },
        function(callback){
            var rsi_promise = new Promise(function(resolve, reject) {
                talib.execute({
                    name: "RSI",
                    inReal: marketData,
                    startIdx: 0,
                    endIdx: marketData.length-1,
                    optInTimePeriod: 9,
                }, function (result) {
                    var length = result.result.outReal.length-1;
                    resolve(result.result.outReal[length]);
                });
            });
            rsi_promise.then(function(resolve, reject){
                rsi = resolve
                callback();
            });
        },
        function(callback){
            var ema_12_promise = new Promise(function(resolve, reject) {
                talib.execute({
                    name: "EMA",
                    inReal: marketData,
                    startIdx: 0,
                    endIdx: marketData.length-1,
                    optInTimePeriod: 12,
                }, function (result) {
                    var length = result.result.outReal.length-1;
                    resolve(result.result.outReal[length]);
                });
            });
            var ema_26_promise = new Promise(function(resolve, reject) {
                talib.execute({
                    name: "EMA",
                    inReal: marketData,
                    startIdx: 0,
                    endIdx: marketData.length-1,
                    optInTimePeriod: 26,
                }, function (result) {
                    var length = result.result.outReal.length-1;
                    resolve(result.result.outReal[length]);
                });
            });
            Promise.all([ema_12_promise, ema_26_promise]).then(function(resolve){
                macd2.short = resolve[0];
                macd2.long = resolve[1];
                diff = 100 * (macd2.short - macd2.long) / ((macd2.short + macd2.long) / 2)
                macd2.diff = diff;
                callback();
            });
        },
        function(callback){
            var tradeable_promise = new Promise(function(resolve, reject) {
                poloniex.myTradableBalances(function(err, data){
                    if (err){
                        reject(err);
                    }
                    resolve(data);
                });
            });
            tradeable_promise.then(function(resolve, reject){
                tradeable_balance = resolve.BTC_ETH
                callback();
            });
        },
        function(callback) {
            var trade = false;
            var mount = tradeable_balance.ETH;
            mount = mount * 0.8;
            //console.log("tadeable mount: "+mount );
            var price_sell = ((price_market * 100000) - 2) / 100000;
            var price_buy = ((price_market * 100000) + 2) / 100000;
            if (position.type !== "none") {
                trade = true;
                if (position.type === "short") {
                    short = true;
                    long = false;
                    var price_optimal = parseFloat(position.basePrice)  - (parseFloat(position.basePrice) * 0.016)
                    status = "close short : "+price_optimal;
                    if (price_market <= price_optimal ){
                        var trade_close_promise = new Promise(function(resolve, reject) {
                            poloniex.closeMarginPosition('BTC_ETH',function(err, data){
                                if (err){
                                    console.log(err);
                                    reject(err);
                                }
                                console.log(data);
                                resolve(data);
                            });
                        });
                        trade_close_promise.then(function(resolve, reject){
                            console.log(resolve);
                            bot.sendMessage(chatId, 'close short: '+price_market);
                            callback();
                        });
                    } else {
                        var stop_loss = parseFloat(position.basePrice) + (parseFloat (position.basePrice) * 0.12)
                        // var stop_loss = parseFloat(last_buy) + (parseFloat (last_buy) * 0.12)
                        if (price_market >= stop_loss) {
                            var trade_close_promise = new Promise(function(resolve, reject) {
                                poloniex.closeMarginPosition('BTC_ETH',function(err, data){
                                    if (err){
                                        console.log(err);
                                        reject(err);
                                    }
                                    console.log(data);
                                    resolve(data);
                                });
                            });
                            trade_close_promise.then(function(resolve, reject){
                                console.log(resolve);
                                bot.sendMessage(chatId, 'close stop loss short: '+price_market);
                                callback();
                            });
                        }
                    }
                }
                if (position.type === "long") {
                    // var price_optimal = position.basePrice + (position.basePrice * 0.016)
                    var price_optimal = parseFloat(position.basePrice)  + (parseFloat(position.basePrice) * 0.016)
                    status = "close long : "+price_optimal
                    // var price_optimal = last_buy + (last_buy * 0.016)
                    if (price_market >= price_optimal) {
                        var trade_close_promise = new Promise(function(resolve, reject) {
                            poloniex.closeMarginPosition('BTC_ETH',function(err, data){
                                if (err){
                                    console.log(err);
                                    reject(err);
                                }
                                console.log(data);
                                resolve(data);
                            });
                        });
                        trade_close_promise.then(function(resolve, reject){
                            console.log(resolve);
                            bot.sendMessage(chatId, 'close long: '+price_market);
                            callback();
                        });
                    } else {
                        // var price_optimal = position.basePrice - (position.basePrice * 0.12)
                        var price_optimal = parseFloat(position.basePrice)  - (parseFloat(position.basePrice) * 0.12)
                        // var price_optimal = last_buy - (last_buy * 0.12);
                        if (price_market <= price_optimal) {
                            var trade_close_promise = new Promise(function(resolve, reject) {
                                poloniex.closeMarginPosition('BTC_ETH',function(err, data){
                                    if (err){
                                        console.log(err);
                                        reject(err);
                                    }
                                    console.log(data);
                                    resolve(data);
                                });
                            });
                            trade_close_promise.then(function(resolve, reject){
                                console.log(resolve);
                                bot.sendMessage(chatId, 'close stop looa long: '+price_market);
                                callback();
                            });
                        }

                    }

                }
            }
            // diff = 100 * (macd_data.macd - macd_data.signal) / ((macd_data.macd + macd_data.signal));
            if (!trade) {
                //console.log("waiting next trade");
                status = "waiting next trade";
                /////////////////////// Short////////////////////////////////////////////////////////////
                if (macd2.short < macd2.long) {
                    if (macd2.diff < -0.35 && macd2.diff > - 0.6 && rsi > 60) {
                        var trade_buy_promise = new Promise(function(resolve, reject) {
                            poloniex.marginSell('BTC_ETH',mount,price_sell,function(err, data){
                                if (err){
                                    console.log(err);
                                    reject(err);
                                }
                                resolve(data);
                            });
                        });
                        trade_buy_promise.then(function(resolve, reject){
                            last_buy = price_sell
                            console.log("margin sell");
                            bot.sendMessage(chatId, 'margin sell placed: '+price_sell);
                            console.log(resolve);
                            callback();
                        });
                    }
                }
                //////////////////////// Long ///////////////////////////////////////////////////////////
                var macd = macd_data.macd * 10000
                var histogram = macd_data.histogram * 10000
                if (histogram < -0.1 && macd2.diff > 0){
                    if ((macd > 0.01 && macd < 0.9 )&& macd2.long < 0.015){
                        var trade_buy_promise = new Promise(function(resolve, reject) {
                            poloniex.marginBuy('BTC_ETH',mount,price_buy,function(err, data){
                                if (err){
                                    console.log(err);
                                    reject(err);
                                }
                                resolve(data);
                            });
                        });
                        trade_buy_promise.then(function(resolve, reject){
                            last_buy = resolve
                            console.log(resolve);
                            bot.sendMessage(chatId, 'margin buy placed: '+price_buy);
                            console.log("margin buy");
                            callback();
                        });
                    }
                }

                if (macd2.short < macd2.long) {
                    if (macd2.diff < -0.45) {
                        var trade_buy_promise = new Promise(function(resolve, reject) {
                            poloniex.marginBuy('BTC_ETH',mount,price_buy,function(err, data){
                                if (err){
                                    console.log(err);
                                    reject(err);
                                }
                                resolve(data);
                            });
                        });
                        trade_buy_promise.then(function(resolve, reject){
                            last_buy = resolve
                            console.log("margin buy");
                            bot.sendMessage(chatId, 'margin buy placed: '+price_buy);
                            console.log(resolve);
                            callback();
                        });
                    }
                }
            }
        }
    ], function(data) {
    });
}, 1 * (60 * 1000));


function getMACD(marketData)
{
    return new Promise(function(resolve, reject) {
        talib.execute({
                    name: "MACD",
                    startIdx: 0,
                    endIdx: marketData.close.length-1,
                    inReal: marketData.close,
                    optInFastPeriod: 12,
                    optInSlowPeriod: 26,
                    optInSignalPeriod: 9
                }, function (result) {
                    var length = result.result.outMACD.length-1;
                    var macd = {
                        macd:result.result.outMACD[length],
                        signal:result.result.outMACDSignal[length],
                        histogram:result.result.outMACDHist[length],
                    };
                    resolve(macd);
                });
    });
}
function getPrice(date_start_str, macd)
{
    poloniex.getTradeHistory('BTC','ETH',date_start_str,date_start_str, function(err, data) {
        if (err){
            console.log('ERROR', err);
            return;
        }
        var leghth_price = data.length-1;
        data = data[leghth_price];
        checkMarket(data, macd)
        return
        // return data;
        // console.log(data);
    })
}

function formatMarket(marketData)
{
    var marketContents = {
            "_comment" : "20120126 AAPL 1min",
            "open": [
            ],
            "close": [
            ],
            "high": [
            ],
            "low": [
            ],
            "volume": [
            ]
        }
    for (var i = 0; i < marketData.length; i++) {
        marketContents.open.push(marketData[i].open);
        marketContents.close.push(marketData[i].close);
        marketContents.high.push(marketData[i].high);
        marketContents.low.push(marketData[i].low);
        marketContents.volume.push(marketData[i].volume);
    };
    return marketContents;
}

function formatMarketBF(marketData)
{
    var new_array = [];
    if (typeof marketData === "undefined") {
        console.log(marketData);
        marketData = [];
    }
    if (marketData.length > 0) {
        for (var i = marketData.length-1; i >= 0 ; i--) {
            new_array.push(marketData[i].price);
        }
    } else {
        console.log("market empty");
    }
    return new_array;
}

function dateBase(date_start_str)
{
    var date = new Date(date_start_str);
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    date.setDate(day - 1);
    date.setHours(hours - 17);
    date.setMinutes(minutes - 30);
    return dateToString(date);
}

function sumDate(date_str, minutes_add)
{
    var temp_date = new Date(date_str);
    var minutes = temp_date.getMinutes();
    // temp_date.setMinutes(minutes + minutes_add);
    return dateToString(temp_date);
}

function dateToString(date)
{
    var str_year = date.getFullYear();
    var str_month = date.getMonth()+1;
    var str_day = date.getDate();
    var str_hours = date.getHours();
    var str_minutes = date.getMinutes();
    if (str_month < 10) {
        str_month = "0"+str_month;
    }
    if (str_day < 10) {
        str_day = "0"+str_day;
    }
    if (str_hours < 10) {
        str_hours = "0"+str_hours;
    }
    if (str_minutes < 10) {
        str_minutes = "0"+str_minutes;
    }
    var str_date = str_year+"-"+str_month+"-"+str_day+" "+str_hours+":"+str_minutes;
    return str_date;
}

function marginBuy()
{
    var trade_buy_promise = new Promise(function(resolve, reject) {
        poloniex.marginBuy('BTC_ETH',mount,price_market,function(err, data){
            if (err){
                console.log(err);
                reject(err);
            }
            console.log(data);
            resolve(data);
        });
    });
    trade_buy_promise.then(function(resolve, reject){
        console.log(resolve);
        callback();
    });
}
