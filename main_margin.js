
var moment = require('moment-timezone');
var talib = require('./node_modules/talib/build/Release/talib');
var Poloniex = require('./lib/poloniex'),
poloniex = new Poloniex(
    '19EVXFQ5-APPNDBHS-MFDIP9QU-XPIIZU0Q',
    'bcddfa38c8dd1ea00ff3be802da81a366a87611f616b3b45a37cf990ea08c0847f3d42c81c49505b18593e858eb3e92adb4c45b2a758f0067418e85b54fe6ca8'
);
var async = require('async');
////////////////////Telegram api ////////////////////////////////
var status = "Trade iniciado";
var TelegramBot = require('node-telegram-bot-api');

var token = '161348950:AAFlSbqkgriD09sSdCqW0J3-jFUY8MABJXY';
// Setup polling way
var bot = new TelegramBot(token, {polling: true});
var chatId = "";
var coin = "BTC_ETH";

console.log("Margin bot iniciado");

bot.onText(/\/start/, function (msg) {
    chatId = msg.chat.id;
    bot.sendMessage(chatId, 'Bot iniciado');
});
bot.onText(/\/status/, function (msg) {
    chatId = msg.chat.id;
    bot.sendMessage(chatId, status);
});
bot.onText(/\/coin/, function (msg) {
    chatId = msg.chat.id;
    bot.sendMessage(chatId, coin);
});


///////////////////////////////////////////////////
var subtractDate = function(date_str, minutes_add)
{
    var temp_date = moment(date_str);
    var minutes = temp_date.get('minute');
    temp_date.subtract(minutes_add, 'minute')
    return temp_date.format("YYYY-MM-DD HH:mm:ss");
}

var sumDate = function(date_str, minutes_add)
{
    var temp_date = moment(date_str);
    var minutes = temp_date.get('minute');
    temp_date.add(minutes_add, 'minute')
    return temp_date.format("YYYY-MM-DD HH:mm:ss");
}

var diffDates = function(date_start_str, date_end_str)
{
    var date_start = new Date(date_start_str);
    var date_end = new Date(date_end_str);
    var interval = date_end.getTime() - date_start.getTime();
    var timeDiff = interval / 1000;
    timeDiff = Math.floor(timeDiff / 60);
    return timeDiff;
}

var marketData = [];
//var diff = diffDates(date_start,date_end);
var count = 0;
var price = 0;
var MACD,RSI,EMA = {short:"",long:"",diff:""},last_buy,trade = false,price_market,tradeable_balance,position,open_orders;
setInterval(function(){
    var date_start = moment().format("YYYY-MM-DD HH:mm:ss");
    var start = subtractDate(date_start, 500);
    var end = date_start;
    var toDay = moment(start).tz('Africa/Bissau').format("YYYY-MM-DD HH:mm:ss Z");
    var toEnd = moment(end).tz('Africa/Bissau').format("YYYY-MM-DD HH:mm:ss Z");
    async.series([
        function(callback){
            var open_orders_promise = new Promise(function(resolve, reject) {
                poloniex.myOpenOrders(coin,function(err, data){
                    if (err){
                        reject(err);
                    }
                    resolve(data)
                });
            });
            open_orders_promise.then(function(resolve, reject){
                open_orders = resolve;
                callback();
            });
        },
        function(callback){
            if (open_orders.length > 0) {
                for (var i = 0; i < open_orders.length; i++) {
                    var orderNumber = open_orders[i].orderNumber;
                    var cancel_orders_promise = new Promise(function(resolve, reject) {
                        poloniex.cancelOrder(coin,orderNumber,function(err, data){
                            if (err){
                                reject(err);
                            }
                            resolve(data)
                        });
                    });
                    cancel_orders_promise.then(function(resolve, reject){
                        callback();
                    });
                }
            } else {
                callback();
            }
        },
        function(callback){
            var price_market_promise = new Promise(function(resolve, reject) {
                poloniex.getCurrencies(function(err, data){
                    if (err){
                        reject(err);
                        // console.log(err);
                    }
                    resolve(data)
                });
            });
            price_market_promise.then(function(resolve, reject){
                // console.log(resolve);
                price_market = resolve.BTC_ETH.last
                callback();
            });
        },
        function(callback){
            var market_promise = new Promise(function(resolve, reject) {
                poloniex.getTradeHistory(coin,toDay,toEnd,function(err, data){
                    if (err){
                        reject(err);
                    }
                    resolve(data);
                });
            });
            market_promise.then(function(resolve, reject){
                var price_market = resolve
                var chartData = [];
                var minute = "";
                for (var i = 0; i < price_market.length; i++) {
                    var date = moment(price_market[i].date)
                    if (minute !== date.minutes()) {
                        minute = date.minutes()
                        chartData.push(price_market[i].rate);
                    }
                }
                // for (var i = price_market.length - 1; i >= 0; i--) {
                //     var date = moment(price_market[i].date)
                //     if (minute !== date.minutes()) {
                //         minute = date.minutes()
                //         chartData.push(price_market[i].rate);
                //     }
                // }
                marketData = chartData;
                callback();
            });
        },
        function(callback){
            var ema_12_promise = new Promise(function(resolve, reject) {
                talib.execute({
                    name: "EMA",
                    inReal: marketData,
                    startIdx: 0,
                    endIdx: marketData.length-1,
                    optInTimePeriod: 12,
                }, function (result) {
                    var length = result.result.outReal.length-1;
                    resolve(result.result.outReal[length]);
                });
            });
            var ema_26_promise = new Promise(function(resolve, reject) {
                talib.execute({
                    name: "EMA",
                    inReal: marketData,
                    startIdx: 0,
                    endIdx: marketData.length-1,
                    optInTimePeriod: 26,
                }, function (result) {
                    var length = result.result.outReal.length-1;
                    resolve(result.result.outReal[length]);
                });
            });
            var macd_promise = new Promise(function(resolve, reject) {
                talib.execute({
                    name: "MACD",
                    startIdx: 0,
                    endIdx: marketData.length-1,
                    inReal: marketData,
                    optInFastPeriod: 12,
                    optInSlowPeriod: 26,
                    optInSignalPeriod: 9
                }, function (result) {
                    var length = result.result.outMACD.length-1;
                    var macd = {
                        macd:result.result.outMACD[length],
                        signal:result.result.outMACDSignal[length],
                        histogram:result.result.outMACDHist[length],
                    };
                    resolve(macd);
                });
            });
            var rsi_promise = new Promise(function(resolve, reject) {
                talib.execute({
                    name: "RSI",
                    inReal: marketData,
                    startIdx: 0,
                    endIdx: marketData.length-1,
                    optInTimePeriod: 9,
                }, function (result) {
                    var length = result.result.outReal.length-1;
                    resolve(result.result.outReal[length]);
                });
            });
            Promise.all([ema_12_promise, ema_26_promise, macd_promise, rsi_promise]).then(function(resolve){
                EMA.short = resolve[0];
                EMA.long = resolve[1];
                diff = 100 * (EMA.short - EMA.long) / ((EMA.short + EMA.long) / 2)
                EMA.diff = diff;
                MACD = resolve[2];
                RSI = resolve[3];
                price = marketData[marketData.length - 1];
                callback();
            });
        },
        function(callback) {
            var margin_position_promise = new Promise(function(resolve, reject) {
                poloniex.myMarginPosition(coin,function(err, data){
                    if (err){
                        reject(err);
                    }
                    resolve(data);
                });
            });
            margin_position_promise.then(function(resolve, reject){
                position = resolve
                callback();
            });
        },
        function(callback){
            var tradeable_promise = new Promise(function(resolve, reject) {
                poloniex.myTradableBalances(function(err, data){
                    if (err){
                        reject(err);
                    }
                    resolve(data);
                });
            });
            tradeable_promise.then(function(resolve, reject){
                tradeable_balance = resolve.BTC_ETH
                callback();
            });
        },
        function(callback) {
            status = "Esperando el proximo trade";
            var trade = false;
            var mount = tradeable_balance.ETH;
            var price_sell = ((price_market * 100000) - 2) / 100000;
            var price_buy  = ((price_market * 100000) + 2) / 100000;
            mount = mount * 1;
            if (position.type !== "none") {
                trade = true;
                if (position.type === "short") {
                    var price_optimal = parseFloat(position.basePrice)  - (parseFloat(position.basePrice) * 0.016)
                    status = "close short : "+price_optimal;
                    if (price_market <= price_optimal ){
                        var trade_close_promise = new Promise(function(resolve, reject) {
                            poloniex.closeMarginPosition(coin,function(err, data){
                                if (err){
                                    console.log(err);
                                    reject(err);
                                }
                                console.log(data);
                                resolve(data);
                            });
                        });
                        trade_close_promise.then(function(resolve, reject){
                            console.log(resolve);
                            bot.sendMessage(chatId, 'close short: '+price_market);
                            callback();
                        });
                    } else {
                        var stop_loss = parseFloat(position.basePrice) + (parseFloat (position.basePrice) * 0.12)
                        if (price_market >= stop_loss) {
                            var trade_close_promise = new Promise(function(resolve, reject) {
                                poloniex.closeMarginPosition(coin,function(err, data){
                                    if (err){
                                        console.log(err);
                                        reject(err);
                                    }
                                    console.log(data);
                                    resolve(data);
                                });
                            });
                            trade_close_promise.then(function(resolve, reject){
                                console.log(resolve);
                                bot.sendMessage(chatId, 'close stop loss short: '+price_market);
                                callback();
                            });
                        } else {
                            callback();
                        }
                    }
                }
                if (position.type === "long") {
                    var price_optimal = parseFloat(position.basePrice)  + (parseFloat(position.basePrice) * 0.016)
                    status = "close long : "+price_optimal
                    if (price_market >= price_optimal) {
                        var trade_close_promise = new Promise(function(resolve, reject) {
                            poloniex.closeMarginPosition(coin,function(err, data){
                                if (err){
                                    console.log(err);
                                    reject(err);
                                }
                                console.log(data);
                                resolve(data);
                            });
                        });
                        trade_close_promise.then(function(resolve, reject){
                            console.log(resolve);
                            bot.sendMessage(chatId, 'close long: '+price_market);
                            callback();
                        });
                    } else {
                        var price_optimal = parseFloat(position.basePrice)  - (parseFloat(position.basePrice) * 0.12);
                        if (price_market <= price_optimal) {
                            var trade_close_promise = new Promise(function(resolve, reject) {
                                poloniex.closeMarginPosition(coin,function(err, data){
                                    if (err){
                                        console.log(err);
                                        reject(err);
                                    }
                                    console.log(data);
                                    resolve(data);
                                });
                            });
                            trade_close_promise.then(function(resolve, reject){
                                console.log(resolve);
                                bot.sendMessage(chatId, 'close stop looa long: '+price_market);
                                callback();
                            });
                        } else {
                            callback();
                        }

                    }
                }
            }
            if (!trade) {
                ////////////////////////////// Short////////////////////////////////////
                if (EMA.short < EMA.long) {
                    if (EMA.diff < -0.35 && EMA.diff > - 0.6 && RSI > 60) {
                        var trade_sell_promise = new Promise(function(resolve, reject) {
                            poloniex.marginSell(coin,mount,price_sell,function(err, data){
                                if (err){
                                    console.log(err);
                                    reject(err);
                                }
                                resolve(data);
                            });
                        });
                        trade_sell_promise.then(function(resolve, reject){
                            last_buy = price_sell
                            bot.sendMessage(chatId, 'margin sell placed: '+price_sell);
                            callback();
                        });
                    } else {
                        callback();
                    }
                } else {
                    callback();
                }
                //////////////////////// Long ///////////////////////////////////////////////////////////
                /*var macd = MACD.macd * 10000
                var histogram = MACD.histogram * 10000
                if (histogram < -0.1 && EMA.diff > 0){
                    if ((macd > 0.01 && macd < 0.9 )&& EMA.long < 0.015){
                        var trade_buy_promise = new Promise(function(resolve, reject) {
                            poloniex.marginBuy(coin,mount,price_buy,function(err, data){
                                if (err){
                                    console.log(err);
                                    reject(err);
                                }
                                resolve(data);
                            });
                        });
                        trade_buy_promise.then(function(resolve, reject){
                            last_buy = resolve
                            bot.sendMessage(chatId, 'margin buy placed: '+price_buy);
                            callback();
                        });
                    } else {
                        callback();
                    }
                } else {
                    if (EMA.short < EMA.long) {
                        if (EMA.diff < -0.45) {
                            var trade_buy_promise = new Promise(function(resolve, reject) {
                                poloniex.marginBuy(coin,mount,price_buy,function(err, data){
                                    if (err){
                                        console.log(err);
                                        reject(err);
                                    }
                                    resolve(data);
                                });
                            });
                            trade_buy_promise.then(function(resolve, reject){
                                last_buy = resolve
                                bot.sendMessage(chatId, 'margin buy placed: '+price_buy);
                                callback();
                            });
                        } else {
                            callback();
                        }
                    } else {
                        callback();
                    }
                }*/
            }
        }
    ]);
    start = sumDate(start, +1);
    end = sumDate(end, + 1);
}, 1 * (60 * 1000));