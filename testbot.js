
# Estrategias
class Strategies
    # RSI
    @rsi: (data, period, last = true) ->
        results = talib.RSI
            inReal: data.close
            startIdx: 0
            endIdx: data.close.length-1
            optInTimePeriod: 9
        if last then _.last(results) else results
    #MACD
    @macd: (data)->
        short = data.ema(12)
        long = data.ema(26)
        diff = 100 * (short - long) / ((short + long) / 2)
        macd = {short:short ,long:long, diff:diff}
        macd
#==========================================================#
class Operations
    # Alcista
    @A: (ins, portfolio, macd2, result, storage, context)->
        macd = result.macd * 10000
        histogram = result.histogram * 10000
        if (histogram < -0.1 && macd2.diff > 0)
            if ((macd > 0.01 && macd < 0.9 )&& macd2.long < 0.015)
                debug "long: "+ins.price
                storage.lastBuyPrice = ins.price
                context.trade = true
                context.trade_buy = true
                # if (Trade.buy(ins, storage))
                    #debug "macd: "+macd
                    #debug "long: "+macd2.long
                    # storage.fast = true
        # lastBuy = storage.lastBuyPrice
        # price_optimal = lastBuy + (lastBuy * context.profit_fast)
        # if (ins.price > price_optimal && storage.fast)
        #     if (Trade.sell(ins))
        #         storage.fast = false
        #nada
    # Bajista
    @B:(macd, ins, portfolio, storage, context)->
        if (macd.short < macd.long)
            if macd.diff < context.buy_treshold
                debug "long: "+ins.price
                storage.lastBuyPrice = ins.price
                context.trade = true
                context.trade_buy = true
                # Trade.buy(ins, storage)
                # storage.buy_b = true
                # storage.buy_a = false
        # else
        #     lastBuy = storage.lastBuyPrice
        #     price_optimal = lastBuy + (lastBuy * context.profit)
        #     if ins.price > price_optimal
        #         Trade.sell(ins)
        #         storage.buy_b = false
    # Stop Loss
    # @stop: (ins, storage, portfolio, context)->
    #         lastBuy = storage.lastBuyPrice
    #         price_optimal = lastBuy - (lastBuy * context.stop_loss)
    #         if (ins.price < price_optimal)
    #             if Trade.sell(ins)
    #                 storage.buy_b = false
    #                 debug "stop loss enter!!"

#==========================================================#
class Wallet
    # Profit
    @profit: (portfolio, ins, storage, context) -> 
        if (storage.btcAmount == undefined) 
            storage.btcAmount = portfolio.positions['btc'].amount
        btcAmount = portfolio.positions['btc'].amount
        if (btcAmount < 0.0001)
            btcAmount = portfolio.positions['maid'].amount * ins.price
        debug "Profit : "+(btcAmount - storage.btcAmount)
        if (storage.buy_b or storage.fast)
            lastBuy = storage.lastBuyPrice
            price_optimal = lastBuy + (lastBuy * context.profit)
            stop_loss = lastBuy - (lastBuy * context.stop_loss)
            debug "Next Sell: "+price_optimal
            debug "Stop Loss: "+stop_loss
        debug "#=======================================#"
init: (context)->
    context.buy_treshold = -0.45
    context.sell_treshold = 0.9
    context.profit = 0.016
    context.profit_fast = 0.02
    context.stop_loss = 0.26
    context.trade = false
    context.trade_sell = false
    context.trade_buy = false
    context.stop = 0
    context.close = 0

handle: (context, data, storage)->

    ins = data.instruments[0]
    macd2 = Strategies.macd(ins)
    rsi = Strategies.rsi(ins)
    result = data.instruments[0].macd(12,26,9)
    if (storage.btcAmount == undefined)
            storage.btcAmount = portfolio.positions['btc'].amount

    plot
        macd: result.macd * 10000
        signal: result.signal * 10000
        histogram: result.histogram * 10000
    if (context.trade)
        if (context.trade_sell)
            lastBuy = storage.lastBuyPrice
            price_optimal = lastBuy - (lastBuy * 0.023)
            if ins.price < price_optimal
                debug "close position : "+ins.price
                storage.btcAmount = storage.btcAmount + (((portfolio.positions['btc'].amount / ins.price) - (portfolio.positions['btc'].amount / lastBuy)) * ins.price)
                context.close++
                context.trade = false
                context.trade_sell = false
            else
                price_optimal = lastBuy + (lastBuy * 0.12)
                if ins.price > price_optimal
                    debug "stop loss enter: "+ins.price
                    storage.btcAmount = storage.btcAmount - (((portfolio.positions['btc'].amount / lastBuy) - (portfolio.positions['btc'].amount / ins.price)) * ins.price)
                    context.stop++
                    context.trade = false
                    context.trade_sell = false
        if (context.trade_buy)
            lastBuy = storage.lastBuyPrice
            price_optimal = lastBuy + (lastBuy * 0.016)
            if ins.price > price_optimal
                debug "close position : "+ins.price
                storage.btcAmount = storage.btcAmount + (((portfolio.positions['btc'].amount / lastBuy) - (portfolio.positions['btc'].amount / ins.price)) * ins.price)
                context.close++
                context.trade = false
                context.trade_buy = false
            else
                price_optimal = lastBuy - (lastBuy * 0.12)
                if ins.price < price_optimal
                    debug "stop loss enter: "+ins.price
                    storage.btcAmount = storage.btcAmount - (((portfolio.positions['btc'].amount / ins.price) - (portfolio.positions['btc'].amount / lastBuy)) * ins.price)
                    context.stop++
                    context.trade = false
                    context.trade_buy = false
    if (!context.trade)
        if (macd2.short < macd2.long)
            if macd2.diff < -0.35 && macd2.diff > - 0.6 && rsi > 60
                debug "short: "+ins.price
                storage.lastBuyPrice = ins.price
                context.trade = true
                context.trade_sell = true
        Operations.A ins, portfolio, macd2 , result, storage ,context
        Operations.B result, ins, portfolio, storage ,context
    #btcAmount = portfolio.positions['btc'].amount
    # debug "stop loss : "+context.stop+" trades: "+context.close+ " btc profit : "+storage.btcAmount

    #Operations.stop ins, storage, portfolio, context

    #Wallet.profit portfolio, ins, storage ,context
