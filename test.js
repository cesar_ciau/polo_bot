var moment = require('moment-timezone');
var talib = require('./node_modules/talib/build/Release/talib');
var Poloniex = require('./lib/poloniex'),
poloniex = new Poloniex(
    '19EVXFQ5-APPNDBHS-MFDIP9QU-XPIIZU0Q',
    'bcddfa38c8dd1ea00ff3be802da81a366a87611f616b3b45a37cf990ea08c0847f3d42c81c49505b18593e858eb3e92adb4c45b2a758f0067418e85b54fe6ca8'
);
var async = require('async');
var date_start = "2016-03-01 16:10";
var date_end = "2016-03-01 16:15";

var subtractDate = function(date_str, minutes_add)
{
    var temp_date = moment(date_str);
    var minutes = temp_date.get('minute');
    temp_date.subtract(minutes_add, 'minute')
    return temp_date.format("YYYY-MM-DD HH:mm:ss");
}

var sumDate = function(date_str, minutes_add)
{
    var temp_date = moment(date_str);
    var minutes = temp_date.get('minute');
    temp_date.add(minutes_add, 'minute')
    return temp_date.format("YYYY-MM-DD HH:mm:ss");
}

var diffDates = function(date_start_str, date_end_str)
{
    var date_start = new Date(date_start_str);
    var date_end = new Date(date_end_str);
    var interval = date_end.getTime() - date_start.getTime();
    var timeDiff = interval / 1000;
    timeDiff = Math.floor(timeDiff / 60);
    return timeDiff;
}

var marketData = [];
var diff = diffDates(date_start,date_end);

var count = 0;
var start = subtractDate(date_start, 50);
var end = date_start;
var coin = "BTC_ETH";
var price = 0;
var MACD,RSI,EMA = {short:"",long:"",diff:""},last_buy,trade = false, type = "";
var toDay = moment(start).tz('Africa/Bissau').format("YYYY-MM-DD HH:mm:ss Z");
var toEnd = moment(end).tz('Africa/Bissau').format("YYYY-MM-DD HH:mm:ss Z");
console.log("diff: "+diff);
/*async.whilst(
    function () {
        return count < diff;
    },
    function (callback) {
        count++;
        async.series([
            function(callback){
                var promise = new Promise(function(resolve,reject){
                    var toDay = moment(start).tz('Africa/Bissau').format("YYYY-MM-DD HH:mm:ss Z");
                    var toEnd = moment(end).tz('Africa/Bissau').format("YYYY-MM-DD HH:mm:ss Z");
                    poloniex.getTradeHistory("BTC_ETH",toDay,toEnd,function(err, data){
                        if (err){
                            reject(err);
                        }
                        resolve(data)
                    });
                });
                promise.then(function(resolve, reject){
                    var price_market = resolve
                    var chartData = [];
                    var minute = "";
                    for (var i = price_market.length - 1; i >= 0; i--) {
                        var date = moment(price_market[i].date)
                        if (minute !== date.minutes()) {
                            minute = date.minutes()
                            chartData.push(price_market[i].rate);
                        }
                    }
                    marketData = chartData;
                    callback();
                });
            },
            function(callback){
                var ema_12_promise = new Promise(function(resolve, reject) {
                    talib.execute({
                        name: "EMA",
                        inReal: marketData,
                        startIdx: 0,
                        endIdx: marketData.length-1,
                        optInTimePeriod: 12,
                    }, function (result) {
                        var length = result.result.outReal.length-1;
                        resolve(result.result.outReal[length]);
                    });
                });
                var ema_26_promise = new Promise(function(resolve, reject) {
                    talib.execute({
                        name: "EMA",
                        inReal: marketData,
                        startIdx: 0,
                        endIdx: marketData.length-1,
                        optInTimePeriod: 26,
                    }, function (result) {
                        var length = result.result.outReal.length-1;
                        resolve(result.result.outReal[length]);
                    });
                });
                var macd_promise = new Promise(function(resolve, reject) {
                    talib.execute({
                        name: "MACD",
                        startIdx: 0,
                        endIdx: marketData.length-1,
                        inReal: marketData,
                        optInFastPeriod: 12,
                        optInSlowPeriod: 26,
                        optInSignalPeriod: 9
                    }, function (result) {
                        var length = result.result.outMACD.length-1;
                        var macd = {
                            macd:result.result.outMACD[length],
                            signal:result.result.outMACDSignal[length],
                            histogram:result.result.outMACDHist[length],
                        };
                        resolve(macd);
                    });
                });
                var rsi_promise = new Promise(function(resolve, reject) {
                    talib.execute({
                        name: "RSI",
                        inReal: marketData,
                        startIdx: 0,
                        endIdx: marketData.length-1,
                        optInTimePeriod: 9,
                    }, function (result) {
                        var length = result.result.outReal.length-1;
                        resolve(result.result.outReal[length]);
                    });
                });
                Promise.all([ema_12_promise, ema_26_promise, macd_promise, rsi_promise]).then(function(resolve){
                    EMA.short = resolve[0];
                    EMA.long = resolve[1];
                    diff = 100 * (EMA.short - EMA.long) / ((EMA.short + EMA.long) / 2)
                    EMA.diff = diff;
                    MACD = resolve[2];
                    RSI = resolve[3];
                    price = marketData[marketData.length - 1];
                    callback();
                });
            },
            function(callback){
                if (trade == false) {
                    console.log("trade");
                    // Short
                    if (EMA.short < EMA.long) {
                        console.log(EMA.diff);
                        if (EMA.diff < -0.35 && EMA.diff > - 0.6 && RSI > 60) {
                            var trade_sell_promise = new Promise(function(resolve, reject) {
                                resolve(price);
                                // poloniex.marginBuy('BTC_ETH',mount,price_buy,function(err, data){
                                //     if (err){
                                //         console.log(err);
                                //         reject(err);
                                //     }
                                //     resolve(data);
                                // });
                            });
                            trade_sell_promise.then(function(resolve, reject){
                                last_buy = resolve
                                // console.log(resolve);
                                // bot.sendMessage(chatId, 'sell buy placed: '+price_buy);
                                console.log("short");
                                type = "sell";
                                trade = true;
                                callback();
                                callback(null, count);
                            });
                            // console.log("short");
                        }
                    }
                    // long
                    var macd = MACD.macd * 10000
                    var histogram = MACD.histogram * 10000
                    if (histogram < -0.1 && EMA.diff > 0){
                        if ((macd > 0.01 && macd < 0.9 )&& EMA.long < 0.015){
                            var trade_buy_promise = new Promise(function(resolve, reject) {
                                // poloniex.marginBuy('BTC_ETH',mount,price_buy,function(err, data){
                                //     if (err){
                                //         console.log(err);
                                //         reject(err);
                                //     }
                                //     resolve(data);
                                // });
                                resolve(price);
                            });
                            trade_buy_promise.then(function(resolve, reject){
                                last_buy = resolve
                                // console.log(resolve);
                                // bot.sendMessage(chatId, 'margin buy placed: '+price_buy);
                                // console.log("margin buy");
                                type = "buy";
                                console.log("long");
                                last_buy = price;
                                trade = true;
                                callback();
                                callback(null, count);
                            });
                        }
                    }
                    if (EMA.short < EMA.long) {
                        if (EMA.diff < -0.45) {
                            var trade_buy_promise = new Promise(function(resolve, reject) {
                                // poloniex.marginBuy('BTC_ETH',mount,price_buy,function(err, data){
                                //     if (err){
                                //         console.log(err);
                                //         reject(err);
                                //     }
                                //     resolve(data);
                                // });
                                resolve(price);
                            });
                            trade_buy_promise.then(function(resolve, reject){
                                last_buy = resolve
                                // console.log(resolve);
                                // bot.sendMessage(chatId, 'margin buy placed: '+price_buy);
                                // console.log("margin buy");
                                type = "buy";
                                console.log("long");
                                last_buy = price;
                                trade = true;
                                callback();
                                callback(null, count);
                            });
                        }
                    }
                } else {
                    console.log("preparado para vender");
                    // if (EMA.short > EMA.long) {
                        if (trade) {
                            if (type == "sell") {
                                var price_optimal = parseFloat(last_buy)  - (parseFloat(last_buy) * 0.016)
                                var stop_loss = parseFloat(last_buy)  + (parseFloat(last_buy) * 0.12)
                                if (price >= price_optimal) {
                                    var promise_close = new Promise(function(resolve,reject){
                                        resolve("close");
                                    });
                                    promise_close.then(function(resolve, reject){
                                        trade = false;
                                        console.log("close sell");
                                        callback();
                                        callback(null, count);
                                    });
                                } else {
                                    if (price <= stop_loss) {
                                        var promise_close = new Promise(function(resolve,reject){
                                            resolve("close");
                                        });
                                        promise_close.then(function(resolve, reject){
                                            trade = false;
                                            console.log("stop loss");
                                            callback();
                                            callback(null, count);
                                        });
                                    } else {
                                        callback();
                                        callback(null, count);
                                    }
                                }
                            }
                            // if (type == "buy"){
                            //     var price_optimal = parseFloat(last_buy)  + (parseFloat(last_buy) * 0.016)
                            //     var stop_loss = parseFloat(last_buy)  - (parseFloat(last_buy) * 0.12)
                            //     if (price >= price_optimal) {
                            //         var promise_close = new Promise(function(resolve,reject){
                            //             resolve("close");
                            //         });
                            //         promise_close.then(function(resolve, reject){
                            //             trade = false;
                            //             callback();
                            //             callback(null, count);
                            //             console.log(" close buy");
                            //         });
                            //     } else {
                            //         if (price <= stop_loss) {
                            //             var promise_close = new Promise(function(resolve,reject){
                            //                 resolve("close");
                            //             });
                            //             promise_close.then(function(resolve, reject){
                            //                 trade = false;
                            //                 callback();
                            //                 callback(null, count);
                            //                 console.log("stop loss");
                            //             });
                            //         }
                            //     }
                            // }
                        }
                    // }
                }
                // callback(null, count);
                // callback();
            }
        ]);
        start = sumDate(start, +1);
        end = sumDate(end, + 1);
        callback(null, count);
    },
    function (err, n) {
    }
);*/
var market_promise = new Promise(function(resolve, reject) {
    poloniex.getTradeHistory(coin,toDay,toEnd,function(err, data){
        if (err){
            reject(err);
        }
        resolve(data);
    });
});
market_promise.then(function(resolve, reject){
    console.log(resolve.length);
    // var price_market = resolve
    // var chartData = [];
    // var minute = "";
    // for (var i = price_market.length - 1; i >= 0; i--) {
    //     var date = moment(price_market[i].date)
    //     if (minute !== date.minutes()) {
    //         minute = date.minutes()
    //         chartData.push(price_market[i].rate);
    //     }
    // }
    // marketData = chartData;
    // callback();
});


